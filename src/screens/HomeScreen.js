import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Counter = () => {
  return (
    <View
      style={{
        width: '100%',
        paddingVertical: 20,
        paddingHorizontal: 10,
        flexDirection: 'row',
        borderWidth: 2,
        marginBottom: 10,
      }}>
      <View
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'red',
        }}>
        <Text style={{ color: 'white', fontWeight: 'bold' }}>-</Text>
      </View>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontWeight: 'bold', fontSize: 20 }}>0</Text>
      </View>
      <View
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'green',
        }}>
        <Text style={{ color: 'white', fontWeight: 'bold' }}>+</Text>
      </View>
    </View>
  );
};

const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <Counter />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
});

export default HomeScreen;
